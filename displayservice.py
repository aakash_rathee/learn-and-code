def display_service(location_data):
    print("There are ", len(location_data["features"]), " location found.")
    places = location_data["features"]
    for place in places:
        print()
        print("Location : ", place["place_name"])
        print("Longitude : ", place["geometry"]["coordinates"][0])
        print("Latitude : ", place["geometry"]["coordinates"][1])