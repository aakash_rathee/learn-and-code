from GEOLocation import Place

class LocationService():
    def __init__(self,place):
        self.place=place

    def get_location(self):
        place_obj=Place(self.place)
        location_webresponse=place_obj.get_location()
        return location_webresponse
