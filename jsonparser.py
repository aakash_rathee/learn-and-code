import json

def json_parser(webresponse):
    result_text=webresponse.text
    result_in_json = json.loads(result_text)
    return result_in_json