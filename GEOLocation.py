from abc import ABC,abstractmethod
import requests
from Exception import InvalidPlaceError, BadRequestError, UnauthorisedAccessError, NoInternetException

class GeoLocation(ABC):
    @abstractmethod
    def get_location(self):
        pass
    #If more methods will come it will be added here

class Place(GeoLocation):
    def __init__(self, place):
        self.place=place
        self.main_api = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+place+'.json'
    
    def get_location(self):
        #api_key=os.environ.get('geo_location_apikey')
        api_key = 'pk.eyJ1IjoidmlrcmFtNzAwIiwiYSI6ImNrY2h2MXM2YjA4cGgyc3A1a2U2cHlyMGwifQ.j8LXhJiwRTJe4fg85waLIw'
        parameters = {'access_token':api_key}
        result = requests.get(self.main_api, params= parameters)
        if result.status_code==403:
            raise BadRequestError
        if result.status_code == 401:
            raise UnauthorisedAccessError
        if result.status_code != 200:
            raise InvalidPlaceError
        else:
            response = result
        return response

