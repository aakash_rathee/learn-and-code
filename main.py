import jsonparser as js
from Location import LocationService
from GEOLocation import GeoLocation
from Exception import InvalidPlaceError, BadRequestError, UnauthorisedAccessError, NoInternetException
import displayservice as ds
import os
import requests

def main():
    print("Enter the Name of the Place to get Corrdinates")
    location=input()
    if len(location):
        pass
    else:
        exit("No Place Entered")
    
    try:
        obj=LocationService(location)
        response=obj.get_location()
        response_in_json=js.json_parser(response)
        ds.display_service(response_in_json)
    except InvalidPlaceError as error:
        print(error.message)
    except UnauthorisedAccessError as error:
        print(error.message)
    except BadRequestError as error:
        print(error.message)
    except:
        print("New Exception Occur: NO INTERNET CONNECTION.")


main()



