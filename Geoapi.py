import json
import requests
import os
from Exception import InvalidPlaceError, BadRequestError, UnauthorisedAccessError, NoInternetException

class Geolocation():
    def __init__(self, place):
        self.place = place
        #api_key=os.environ.get('geo_location_apikey')
        #self.API_key=os.environ.get('geo_location_apikey')
        # self.API_key = 'pk.eyJ1IjoidmlrcmFtNzAwIiwiYSI6ImNrY2h2MXM2YjA4cGgyc3A1a2U2cHlyMGwifQ.j8LXhJiwRTJe4fg85waLIw'
        self.main_api = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+place+'.json'
    
    def json_parser(self, web_response):
        response_in_json = json.loads(web_response.text)
        return response_in_json

    def get_coordinates(self):
        api_key=os.environ.get('geo_location_apikey')
        parameters = {'access_token':api_key}
        try:
            web_response = requests.get(url=self.main_api, params=parameters)
            if web_response.status_code != 200:
                raise (GEOException("Enter place is not valid"))
        except GEOException as NoValidPlace:
            print("New Exception Occure: ", NoValidPlace.value)
        response_in_json = self.json_parser(web_response)
        return response_in_json
        
    def print_coordinates(self, json_data):
        print("There are ", len(json_data["features"]), " location found for the keyword "+self.place+".")
        places = json_data["features"]
        for place in places:
            print()
            print("Location : ", place["place_name"])
            print("Longitude : ", place["geometry"]["coordinates"][0])
            print("Latitude : ", place["geometry"]["coordinates"][1])
        
