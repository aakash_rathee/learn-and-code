#!/usr/bin/env python
# coding: utf-8

# In[63]:


import requests
import json


# In[90]:


class Geolocation():
    def __init__(self,place):
        self.place=place
        self.API_key='pk.eyJ1IjoidmlrcmFtNzAwIiwiYSI6ImNrY2h2MXM2YjA4cGgyc3A1a2U2cHlyMGwifQ.j8LXhJiwRTJe4fg85waLIw'
        self.main_api='https://api.mapbox.com/geocoding/v5/mapbox.places/'+place+'.json'
    
    def json_parser(self,web_response):
        response_in_json=json.loads(web_response.text)
        return response_in_json
        
    def get_coordinates(self):
        parameters={'access_token':self.API_key}
        #try:
        web_response=requests.get(url=self.main_api, params=parameters)
        #print(web_response.text)
        response_in_json=self.json_parser(web_response)
        return response_in_json
        #except:
        print("EXCEPTION OCCUR: No Internet Connection Available. Check Your Network")
        
    def print_coordinates(self,json_data):
        #print(json_data["features"])
        print("There are ",len(json_data["features"]), " location found for the keyword "+self.place+".")
        places=json_data["features"]
        for place in places:
            print()
            print("Location : ",place["place_name"])
            print("Longitude : ",place["geometry"]["coordinates"][0])
            print("Latitude : ",place["geometry"]["coordinates"][1])
        


# In[91]:


def main():
    print("Enter the Name of the Place to get Corrdinates")
    location=input()
    obj=Geolocation(location)
    data=obj.get_coordinates()
    obj.print_coordinates(data)
    
    


# In[92]:


main()


# In[40]:


main()


# In[ ]:




