class GEOException(Exception):
    pass
    
class InvalidPlaceError(GEOException):
    '''Exception class for negative amount'''
    def __init__(self):
        self.message = 'PLACE NOT FOUND'
class BadRequestError(GEOException):
    '''Exception class for negative amount'''
    def __init__(self):
        self.message = 'BAD REQUEST'
class UnauthorisedAccessError(GEOException):
    '''Exception class for negative amount'''
    def __init__(self):
        self.message = 'UNAUTHORISED ACCESS'
class NoInternetException(GEOException):
    def __init__(self):
        self.message='No Internet Connection'